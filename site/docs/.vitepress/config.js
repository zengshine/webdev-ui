export default {
  themeConfig: {
    siteTitle: "vitepress",
    nav: [{ text: "Guid" }, { text: "Component", link: "components/button/" }],
  },
  socialLinks: [
    { icon: "github", link: "https://gitlab.com/zengshine/webdev-ui.git" },
  ],
  sidebar: {
    "/guild/": [
      {
        text: "Basic",
        items: [
          {
            text: "Install",
            link: "guide/installation",
          },
          {
            text: "Quick start",
            link: "guide/start",
          },
        ],
      },
    ],
    "/components/": [
      {
        text: "Basic Components",
        items: [
          {
            text: "Button",
            link: "/components/button",
          },
        ],
      },
    ],
  },
};
