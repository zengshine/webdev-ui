import download from "download-git-repo";
import chalk from "chalk";
import ora from "ora";
import shell from "shelljs";

export default (remote, name, option) => {
  const downSpinner = ora("parsing the template...").start();
  shell.cd(process.cwd());
  return new Promise((resolve, reject) => {
    const res = shell.exec(remote);
    console.log("res=========================>", res);
    downSpinner.succeed(chalk.green("download success"));
    resolve();
  });
};
