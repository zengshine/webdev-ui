import commandLineArgs from "command-line-args";
import prompts from "prompts";
import gitClone from "./git-clone.js";
import { readFile } from "fs/promises";

const promptsOptions = [
  {
    type: "text",
    name: "name",
    message: "please enter the project's name",
  },
  {
    type: "select",
    name: "template",
    message: "请选择一个模板",
    choices: [
      {
        title: "nounknow-ui",
        value: 1,
      },
      {
        title: "webdev-ui",
        value: 2,
      },
    ],
  },
];

const pkg = JSON.parse(
  await readFile(new URL("./package.json", import.meta.url))
);

const optionDefinitions = [
  {
    name: "version",
    alias: "v",
    type: Boolean,
  },
];

const options = commandLineArgs(optionDefinitions);

console.log("options=========================>", options);

const remoteList = {
  1: "https://gitlab.com/zengshine/webdev-ui.git",
  2: "https://gitlab.com/zengshine/webdev-ui.git",
};

export default async function downloadTemplate() {
  const res = await prompts(promptsOptions);

  console.log("res=========================>", res);

  if (!res.name || !res.template) return;

  gitClone(`git clone ${remoteList[res.template]}`, res.name, { clone: true });
}
