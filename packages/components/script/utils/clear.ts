import fs from "fs";

import { resolve } from "path";

import { pkgPath } from "./paths";

const preservedFiles: string[] = [];

const clearFiles = async (path: string) => {
  let files: string[] = [];

  if (!fs.existsSync(path)) return;

  files = fs.readdirSync(path);

  files.forEach(async (file) => {
    const filePath = resolve(path, file);
    if (fs.statSync(filePath).isDirectory()) {
      if (file !== "node_modules") await clearFiles(filePath);
    } else {
      if (!preservedFiles.includes(file)) {
        fs.unlinkSync(filePath);
      }
    }
  });

  if (path !== `${pkgPath}/dist`) fs.rmdirSync(path);
};

export default clearFiles;
