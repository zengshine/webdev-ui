import run from "../utils/run";

import { pkgPath } from "../utils/paths";

import { series } from "gulp";

export async function publishComponent() {
  run("release-it", `${pkgPath}/components`);
}

export default series(async () => publishComponent());
