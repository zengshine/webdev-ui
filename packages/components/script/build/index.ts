import clearFiles from "../utils/clear";

import { series, parallel, src, dest } from "gulp";
import less from "gulp-less";
import autoPrefixer from "gulp-autoprefixer";

import { pkgPath, componentPath } from "../utils/paths";
import run from "../utils/run";

export function removeDist() {
  return clearFiles(`${pkgPath}/components/dist`);
}

export function buildStyle() {
  return src(`${componentPath}/src/**/style/**.less`)
    .pipe(less())
    .pipe(autoPrefixer())
    .pipe(dest(`${pkgPath}/components/dist/cjs/src`))
    .pipe(dest(`${pkgPath}/components/dist/es/src`));
}

export function buildComponent() {
  run("pnpm run build", componentPath);
}

export default series(
  async () => {
    removeDist();
  },
  async () => {
    await buildComponent();
  },
  async () => {
    buildStyle();
  }
);
