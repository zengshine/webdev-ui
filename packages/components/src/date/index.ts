import _Component from './date.vue';

import type { App } from 'vue';
import type { SFCWithInstall } from '../../typings/index';

const withInstall = <T>(comp: T) => {
  (comp as SFCWithInstall<T>).install = (app: App) => {
    const name = (comp as any).name;

    app.component(name, comp as SFCWithInstall<T>);
  };

  return comp as SFCWithInstall<T>;
};

export const Date = withInstall(_Component);

export default Date;
