import { describe, expect, it } from 'vitest';

import { mount } from '@vue/test-utils';
import button from '../button.vue';

describe('component test', () => {
  it('should be component test', () => {
    expect('component' + ' test').toBe('component test');
  });
});

describe('test button', () => {
  it('should render slot', () => {
    const wrapper = mount(button, {
      slots: {
        default: 'button slot'
      }
    });

    expect(wrapper.text()).toContain('button slot');
  });

  it('should have a type', () => {
    const wrapper = mount(button, {
      props: {
        type: 'middle'
      }
    });

    expect(wrapper.classes()).toContain('button-size--middle');
  });
});
