import type { App, Plugin } from "vue";

export type SFCWithInstall<T> = T & Plugin;
