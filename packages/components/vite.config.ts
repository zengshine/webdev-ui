/// <reference types="vitest" />

import { defineConfig } from 'vite';

import vue from '@vitejs/plugin-vue';
import DefineOptions from 'unplugin-vue-define-options/vite';
import dts from 'vite-plugin-dts';
import vueJsx from '@vitejs/plugin-vue-jsx';

export default defineConfig({
  build: {
    outDir: 'es',
    minify: false,
    emptyOutDir: false,
    rollupOptions: {
      external: ['vue', /\.less/],
      output: [
        {
          format: 'umd',
          name: '[name].umd.js',
          exports: 'named',
          globals: {
            vue: 'Vue'
          },
          dir: 'dist/umd'
        },
        {
          format: 'es',
          entryFileNames: '[name].mjs',
          preserveModules: true,
          exports: 'named',
          dir: 'dist/es'
        },
        {
          format: 'cjs',
          entryFileNames: '[name].js',
          preserveModules: true,
          exports: 'named',
          dir: 'dist/cjs'
        }
      ]
    },
    lib: {
      entry: './index.ts'
      // name: "webdev",
      // fileName: "webdev",
      // formats: ["es", "umd", "cjs"],
    }
  },

  plugins: [
    vue(),
    vueJsx(),
    dts({
      entryRoot: './src',
      outputDir: ['./dist/es/src', './dist/cjs/src'],
      tsConfigFilePath: '../../tsconfig.json'
    }),
    {
      name: 'lessStyle',
      generateBundle(config, bundle) {
        const keys = Object.keys(bundle);

        for (const key of keys) {
          const bundler: any = bundle[key as any];

          this.emitFile({
            type: 'asset',
            fileName: key,
            source: bundler.code.replace(/\.less/g, '.css')
          });
        }
      }
    },
    DefineOptions()
  ],
  test: {
    environment: 'happy-dom'
  }
});
