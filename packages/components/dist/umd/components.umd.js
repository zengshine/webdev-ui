(function(global, factory) {
  typeof exports === "object" && typeof module !== "undefined" ? factory(exports, require("vue"), require("./src/button/style/index.css")) : typeof define === "function" && define.amd ? define(["exports", "vue", "./src/button/style/index.css"], factory) : (global = typeof globalThis !== "undefined" ? globalThis : global || self, factory((global["[name]"] = global["[name]"] || {}, global["[name]"].umd = global["[name]"].umd || {}, global["[name]"].umd.js = {}), global.Vue));
})(this, function(exports2, vue) {
  "use strict";
  const __default__ = vue.defineComponent({ name: "WebdevButton" });
  const _sfc_main = /* @__PURE__ */ vue.defineComponent({
    ...__default__,
    props: {
      type: { default: "small" }
    },
    setup(__props) {
      const props = __props;
      const classNames = vue.computed(() => {
        return `button-size--${props.type}`;
      });
      return (_ctx, _cache) => {
        return vue.openBlock(), vue.createElementBlock("div", {
          class: vue.normalizeClass([vue.unref(classNames)])
        }, "Button", 2);
      };
    }
  });
  const withInstall = (comp) => {
    comp.install = (app) => {
      const name = comp.name;
      app.component(name, comp);
    };
    return comp;
  };
  const Button = withInstall(_sfc_main);
  const components = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
    __proto__: null,
    Button
  }, Symbol.toStringTag, { value: "Module" }));
  const index = {
    install: (app) => {
      for (let comp in components) {
        app.use(components[comp]);
      }
    }
  };
  exports2.Button = Button;
  exports2.default = index;
  Object.defineProperties(exports2, { __esModule: { value: true }, [Symbol.toStringTag]: { value: "Module" } });
});
