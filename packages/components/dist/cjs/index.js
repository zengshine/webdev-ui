"use strict";
Object.defineProperties(exports, { __esModule: { value: true }, [Symbol.toStringTag]: { value: "Module" } });
const index$3 = require("./src/index.js");
const index$1 = require("./src/button/index.js");
const index$2 = require("./src/date/index.js");
const index = {
  install: (app) => {
    for (const comp in index$3) {
      app.use(index$3[comp]);
    }
  }
};
exports.Button = index$1.Button;
exports.Date = index$2.Date;
exports.default = index;
