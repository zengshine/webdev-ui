import type { SFCWithInstall } from "../../typings/index";
export declare const Button: SFCWithInstall<import("vue").DefineComponent<{
    type: {
        type: import("vue").PropType<string | undefined>;
        required: false;
        default: string;
    };
}, {}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    type: {
        type: import("vue").PropType<string | undefined>;
        required: false;
        default: string;
    };
}>>, {
    type: string | undefined;
}>>;
export default Button;
