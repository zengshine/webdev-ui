import * as index$1 from "./src/index.mjs";
import { Button } from "./src/button/index.mjs";
import { Date } from "./src/date/index.mjs";
const index = {
  install: (app) => {
    for (const comp in index$1) {
      app.use(index$1[comp]);
    }
  }
};
export {
  Button,
  Date,
  index as default
};
