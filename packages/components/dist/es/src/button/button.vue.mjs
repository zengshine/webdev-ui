import { defineComponent, computed, openBlock, createElementBlock, normalizeClass, unref, createTextVNode, renderSlot } from "vue";
import "./style/index.css";
const __default__ = defineComponent({ name: "WebdevButton" });
const _sfc_main = /* @__PURE__ */ defineComponent({
  ...__default__,
  props: {
    type: { default: "small" }
  },
  setup(__props) {
    const props = __props;
    const classNames = computed(() => {
      return `button-size--${props.type}`;
    });
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("div", {
        class: normalizeClass([unref(classNames)])
      }, [
        createTextVNode(" Button "),
        renderSlot(_ctx.$slots, "default")
      ], 2);
    };
  }
});
export {
  _sfc_main as default
};
