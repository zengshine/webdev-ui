import * as components from "./index";

declare module "@vue/runtime-core" {
  export interface GlobalComponents {
    WebdevButton: typeof components.Button;
  }
}

export {};
