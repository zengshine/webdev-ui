import { createApp } from "vue";
import WebdevUI from "@nounknow/ui";
import App from "./app.vue";

const app = createApp(App);

app.use(WebdevUI);

app.mount("#app");
